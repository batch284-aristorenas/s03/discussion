--  Cretae Values for the tables

-- [Section] Inserting Records

INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO artists (name) VALUES ("Psy");


-- Inserting in multiple columns
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);


-- Inserting songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 4); -- This will result to an error because the album_id does not exist to its reference table

-- corrected
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 179, "OPM", 2);
-- Data has been truncated for column with a name 'length' because it does not corresponds to the capacity of minutes and seconds, maximum seconds for the time is 60, so 79 is not valid.

-- corrected
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 200, "OPM", 2);


-- [Section] Selecting Records

-- Displays the song name and genre of all the songs
SELECT song_name, genre FROM songs;


-- Displays ALL column and column values of table 'songs'
SELECT * FROM songs

-- song_name from table songs where genre = OPM
SELECT song_name FROM songs WHERE genre = "OPM";


-- We can use AND or OR for multiple expressions in the WHERE clauses
-- Displays the song_name and length of the OPM songs that are more than 2 mins and 40 sec
SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";


-- [Section] Updating Records
-- update --table_name SET column = value
-- means set all songs to 240 where the title = Kundiman
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";



-- [Section] Deleting Records

DELETE FROM artists WHERE name = "Psy";
-- ERROR
-- Beacause the row is used as reference for another table, it could not be deleted


DELETE FROM songs WHERE genre = "OPM" AND length > 159;